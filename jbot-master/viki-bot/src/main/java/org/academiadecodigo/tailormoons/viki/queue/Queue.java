package org.academiadecodigo.tailormoons.viki.queue;

import java.util.LinkedList;

public class Queue {

    private LinkedList<String> queue = new LinkedList<>();
    private String lunchTime = "";


    public String startQueue(String command) {

        command = command.toLowerCase();

        String[] commands = command.split(" ");

        return execute(commands);
    }


    private String execute(String[] commands) {

        switch (commands[0]) {

            case "ping":
                return "pong (I love to play this game =) )";

            case "stop":
                //queue.clear();
                return "Stopping program...\n\n" + print("");

            case "pop":
                String lastPop = pop();
                return print(lastPop);

            case "push":
                push(commands[1]);
                return print("");

            case "check":
                return print("");

            case "remove":
                remove(commands[1]);
                return print("");

            case "help":
                return printCommands();


            case "hello":
                return "Hello, how are you today?";

            case "poop":
                return ":shit:";

            case "cerveja":
                return ":beer:";

            case "almoço":
                if (commands.length > 1) {
                    if (commands[1].equals("reset")) {
                        lunchTime = "";
                        return "Hora de almoço \"resetada\".";
                    }
                }

                if (lunchTime.isEmpty()) {
                    lunchTime = commands[1];
                    return "Hora de almoço registado, não se atrasem meninos! Quem chegar atrasado dança!";
                }
                return "A hora de almoço acaba às: " + lunchTime + ". Quem chegar atrasado... :dancer: :dancers: :chicken_dance: :mario_luigi_dance: :banana_dance: :cool_parrot_shuffling:";
/*
            case "parte":
                return "Só tu me consegues partir o coração Diogo :heart_eyes:";
*/

        }

        return "Can't find \"" + commands[0] + "\". Try viki help\n\n" + print("");
    }


    private String pop() {
        String lastPop = "";
        if (queue.size() > 0) {
            lastPop = queue.peekFirst();
            queue.removeFirst();
        }
        return lastPop;
    }


    private void push(String name) {
        queue.addLast(name);
    }


    private void remove(String name) {
        queue.removeFirstOccurrence(name);
    }


    private String print(String lastPop) {
        String message = "";

        if (queue.size() == 0) {
            message = lastPop.isEmpty() ? "[EMPTY QUEUE]" : "NEXT: " + lastPop + "\n[EMPTY QUEUE]";
            return message;
        }

        for (String name : queue) {
            message += name + " < ";
        }
        message = message.substring(0, message.length() - 2);


        if (!lastPop.equals("")) {
            return "NEXT: " + lastPop + "\nQUEUE: " + message;
        }

        return message;
    }


    private String printCommands() {
        return "pop - Remove first member on the queue.\n" +
                "push {names} - Add one or more names to the end of the queue.\n" +
                "remove [name] - Remove a name from the queue.\n" +
                "check - Print the actual queue.\n" +
                "stop - Meh.. who the f**k would use this command?!\n" +
                "poop - Try and discover\n" +
                "ping - She loves to play this game\n" +
                "cerveja - Get a free beer :wink: \n" +
                "almoço [{time}|reset] - Set/Get lunch time. Don't be late!";

    }

}
